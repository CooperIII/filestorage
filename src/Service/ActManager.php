<?php

namespace App\Service;

use App\Entity\Act;
use App\Entity\Build;
use App\Repository\ActRepository;
use App\Repository\BuildRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\DateTime;

class ActManager extends AbstractManager
{
    private $params;

    /** @var  ActRepository */
    protected $repository;

    public function __construct(EntityManagerInterface $em, ActRepository $repository, ParameterBagInterface $params)
    {
        parent::__construct($em, $repository);
        $this->params = $params;
    }

    public function add($codeBuild, $code, $status, $month, UploadedFile $file)
    {
        /** @var Act $act */
        $act = $this->repository->findOneBy(['build' => $codeBuild, 'code' => $code, 'status' => $status]);
        if (empty($act)) {
            $fileName = tempnam($this->params->get('dir_files'), 'act');
            rename($file->getRealPath(), $fileName);
            $act = new Act($codeBuild, $code, $status, new \DateTime($month), basename($fileName));
            $this->save($act);
        } else {
            $act->setStatus($status);
            $this->save($act);
            rename($file->getRealPath(), $this->params->get('dir_files').DIRECTORY_SEPARATOR.$act->getFile());
        }

        return file_exists($this->params->get('dir_files').DIRECTORY_SEPARATOR.$act->getFile());
    }

    public function getFile($code, $status)
    {
        $acts = $this->repository->findBy(['code' => $code, 'status' => $status], ['added' => 'desc']);
        if (empty($acts)) {
            return false;
        }
        /** @var Act $act */
        $act = $acts[0];

        return $this->params->get('dir_files').DIRECTORY_SEPARATOR.$act->getFile();
    }

    /**
     * @param $codeBuild
     *
     * @return string
     */
    public function getList($codeBuild)
    {
        $acts = $this->repository->findBy(['build' => $codeBuild]);
        $str = '';
        /** @var Act $act */
        foreach ($acts as $act) {
            $str .= sprintf(
                    '%s~%s~%s~%s~%s',
                    $act->getCode(),
                    $act->getBuild(),
                    $act->getMonth()->format('Y-m-d'),
                    $act->getAdded()->format('Y-m-d'),
                    $act->getStatus()
                ).'#';
        }

        return $str;
    }

    /**
     * @param string $code
     */
    public function delete($code)
    {
        /** @var Act $act */
        $acts = $this->repository->findBy(['code'=>$code]);
        foreach ($acts as $act) {
            $file = $this->params->get('dir_files').DIRECTORY_SEPARATOR.$act->getFile();
            if (file_exists($file)) {
                unlink($file);
            }
            $this->remove($act);
            $this->flush();
        }
    }
}