<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Service\ActManager;
use App\Service\BuildManager;
use App\Service\UserManager;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController
{
    private $userRepository;

    private $userManager;

    public function __construct(UserRepository $userRepository, UserManager $userManager)
    {
        $this->userRepository = $userRepository;
        $this->userManager = $userManager;
    }

    public function status(Request $request)
    {

        $status = $this->userManager->getStatus(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        );

        if (is_null($status)) {
            return new Response('', 403);
        } else {
            return new Response((string)$status, 200);
        }
    }
}