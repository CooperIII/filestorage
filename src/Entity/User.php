<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="users", uniqueConstraints={@UniqueConstraint(name="username",columns={"unn", "comp"})})
 */
class User
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(name="unn", type="integer", nullable=false)
     */
    protected $unn;

    /**
     * @ORM\Column(name="comp", type="integer", nullable=false)
     */
    protected $comp;

    /**
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    protected $password;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    protected $status;


    public function __construct(int $unn, int $comp, string $password, int $status)
    {
        $this->unn = $unn;
        $this->comp = $comp;
        $this->password = password_hash($password, PASSWORD_DEFAULT);
        $this->status = $status;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUnn(): ?int
    {
        return $this->unn;
    }

    public function setUnn(int $unn): self
    {
        $this->unn = $unn;

        return $this;
    }

    public function getComp(): ?int
    {
        return $this->comp;
    }

    public function setComp(int $comp): self
    {
        $this->comp = $comp;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

}
