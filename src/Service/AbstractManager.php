<?php

namespace App\Service;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class AbstractManager
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var ServiceEntityRepositoryInterface */
    protected $repository;

    public function __construct(EntityManagerInterface $em, ServiceEntityRepositoryInterface $repository)
    {
        $this->em = $em;
        $this->repository = $repository;
    }

    /**
     * @param object $entity
     * @param bool $flush
     */
    public function save($entity, $flush = true)
    {
        $this->em->persist($entity);
        if ($flush) {
            $this->em->flush();
        }
    }

    public function flush()
    {
        $this->em->flush();
    }

    /**
     * @param string|null $entity
     *
     * @return void
     */
    public function clear($entity = null)
    {
        $this->em->clear($entity);
    }

    /**
     * @param object $object
     *
     * @return void
     */
    public function remove($object)
    {
        $this->em->remove($object);
    }
}