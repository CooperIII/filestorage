<?php

namespace App\Command;

use App\Entity\User;
use App\Service\UserManager;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UserCommand
 * @package App\Command
 */
class UserCommand extends Command
{
    protected static $defaultName = 'user:create';

    /** @var string */
    private $dirData;

    /** @var  UserManager */
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('unn')
            ->addArgument('comp')
            ->addArgument('password')
            ->addArgument('status')
            ->setDescription('user create');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $unn = $input->getArgument('unn');
        $comp = $input->getArgument('comp');
        $password = $input->getArgument('password');
        $status = $input->getArgument('status');
        try {
            $this->userManager->create($unn, $comp, $password, $status);
            $output->writeln('user created');
        }catch (\Exception $e)
        {
            $output->writeln($e->getMessage());
        }

        return 0;

    }
}