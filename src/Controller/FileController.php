<?php

namespace App\Controller;

use App\Service\ActManager;
use App\Service\BuildManager;
use App\Service\UserManager;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FileController extends AbstractController
{
    private $buildManager;

    private $userManager;

    private $actManager;

    public function __construct(BuildManager $buildManager, UserManager $userManager, ActManager $actManager)
    {
        $this->buildManager = $buildManager;
        $this->userManager = $userManager;
        $this->actManager = $actManager;
    }

    public function tester()
    {
        return new Response(
            '<html><body>
        <form action="/api/builds/1/upload" method="post"  enctype="multipart/form-data" >
          <div>
          <label>Загрузка КЦ на сервер</label>
      <input type="text" name="unn" placeholder="unn">
      <input type="text" name="comp" placeholder="comp">
      <input type="text" name="password" placeholder="password">
      <input type="text" name="name" placeholder="name">
      <input type="text" name="short" placeholder="short">
      <input type="text" name="list" placeholder="list">
      <input type="text" name="status" placeholder="status">
   <input type="file" id="file" name="file">
 </div>
 <div>
   <button>Submit</button>
 </div>
</form>
        <form action="/api/builds/1/download" method="post">
          <div>
          <label>Загрузка КЦ с сервера</label>
      <input type="text" name="unn" placeholder="unn">
      <input type="text" name="comp" placeholder="comp">
      <input type="text" name="password" placeholder="password">
      <input type="text" name="status" placeholder="status">
 </div>
 <div>
   <button>Submit</button>
 </div>
</form>

        <form action="/api/acts/1/101/upload" method="post"  enctype="multipart/form-data" >
          <div>
          <label>Загрузка С2Б на сервер</label>
      <input type="text" name="unn" placeholder="unn">
      <input type="text" name="comp" placeholder="comp">
      <input type="text" name="password" placeholder="password">
      <input type="text" name="status" placeholder="status">
      <input type="text" name="month" placeholder="month">
   <input type="file" id="file" name="file">
 </div>
 <div>
   <button>Submit</button>
 </div>
</form>

        <form action="/api/acts/101/download" method="post">
          <div>
          <label>Загрузка С2Б с сервера</label>
      <input type="text" name="unn" placeholder="unn">
      <input type="text" name="comp" placeholder="comp">
      <input type="text" name="password" placeholder="password">
      <input type="text" name="status" placeholder="status">
 </div>
 <div>
   <button>Submit</button>
 </div>
</form>


        <form action="/api/builds/list" method="post">
          <div>
          <label>Список КЦ</label>
      <input type="text" name="unn" placeholder="unn">
      <input type="text" name="comp" placeholder="comp">
      <input type="text" name="password" placeholder="password">
      <input type="text" name="status" placeholder="status">
 </div>
 <div>
   <button>Submit</button>
 </div>
</form>
        
        <form action="/api/acts/1/list" method="post">
          <div>
          <label>Список С2Б</label>
      <input type="text" name="unn" placeholder="unn">
      <input type="text" name="comp" placeholder="comp">
      <input type="text" name="password" placeholder="password">
      <input type="text" name="status" placeholder="status">
 </div>
 <div>
   <button>Submit</button>
 </div>
</form>
        </body></html>'
        );
    }

    public function buildUpload($code, Request $request)
    {
        $info = print_r($request->request->all(), true);
        file_put_contents($this->getParameter('dir_files').DIRECTORY_SEPARATOR.'log.txt', $info);
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }
        try {
            $size = $this->buildManager->add(
                $code,
                $request->request->get('name'),
                $request->request->get('short'),
                $request->request->get('list'),
                $request->request->get('status'),
                $request->files->get('file')
            );

            return new Response($size, $size > 0 ? 200 : 400);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 400);
        }
    }

    public function buildDownload($code, Request $request)
    {
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }
        try {
            $file = $this->buildManager->getFile($code, $request->request->get('status'));

            if (empty($file) || file_exists($file) === false) {
                return new Response('file not found', 404);
            }

            return new BinaryFileResponse($file, 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 400);
        }
    }

    public function buildDownloadId($id, Request $request)
    {
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }
        try {
            $file = $this->buildManager->getFileById($id);

            if (empty($file) || file_exists($file) === false) {
                return new Response('file not found', 404);
            }

            return new BinaryFileResponse($file, 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 400);
        }
    }

    public function buildList(Request $request)
    {
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }

        return new Response($this->buildManager->getList((int)$request->request->get('status')), 200);
    }

    public function buildListVersion($code, Request $request)
    {
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }

        return new Response($this->buildManager->getVersion($code, (int)$request->request->get('status')), 200);
    }

    public function actUpload($codeBuild, $code, Request $request)
    {
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }
        try {
            $this->actManager->add(
                $codeBuild,
                $code,
                $request->request->get('status'),
                $request->request->get('month'),
                $request->files->get('file')
            );

            return new Response('OK', 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 400);
        }
    }

    public function actDownload($code, Request $request)
    {
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }
        try {
            $file = $this->actManager->getFile($code, $request->request->get('status'));
            if (empty($file) || file_exists($file) === false) {
                return new Response('file not found', 404);
            }

            return new BinaryFileResponse($file, 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 400);
        }
    }

    public function actList($codeBuild, Request $request)
    {
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }

        return new Response($this->actManager->getList($codeBuild), 200);
    }

    public function log()
    {
        return new Response(file_get_contents($this->getParameter('dir_files').DIRECTORY_SEPARATOR.'log.txt'), 200);
    }


    public function actDelete($code, Request $request)
    {
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }
        try {
            $this->actManager->delete($code);

            return new Response('OK', 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 400);
        }
    }


    public function buildDelete($code, Request $request)
    {
        if (!$this->userManager->check(
            $request->request->get('unn'),
            $request->request->get('comp'),
            $request->request->get('password')
        )
        ) {
            return new Response('Not find user and password', 403);
        }
        try {
            $this->buildManager->delete($code, (int)$request->request->get('status'));

            return new Response('OK', 200);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 400);
        }
    }
}