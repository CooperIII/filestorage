<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserManager extends AbstractManager
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var UserRepository */
    protected $repository;

    public function __construct(EntityManagerInterface $em, UserRepository $repository)
    {
        parent::__construct($em, $repository);
    }

    /**
     * @param int    $unn
     * @param int    $comp
     * @param string $password
     *
     * @return User
     * @throws \Exception
     */
    public function create($unn, $comp, $password, $status)
    {
        /** @var User $user */
        $user = $this->repository->findOneBy(['unn' => $unn, 'comp' => $comp]);
        if (!is_null($user)) {
            throw new \Exception('duplicate user name');
        }
        $user = new User($unn, $comp, $password, $status);
        $this->save($user, true);

        return $user;
    }

    public function check($unn, $comp, $password)
    {
        /** @var User $user */
        $user = $this->repository->findOneBy(['unn' => $unn, 'comp' => $comp]);
        if (!is_null($user)) {
            return password_verify($password, $user->getPassword());
        }

        return false;
    }

    /**
     * @param $unn
     * @param $comp
     * @param $password
     *
     * @return int|null
     */
    public function getStatus($unn, $comp, $password)
    {
        if(!$this->check($unn, $comp, $password)){
            return null;
        }
        /** @var User $user */
        $user = $this->repository->findOneBy(['unn' => $unn, 'comp' => $comp]);

        return $user->getStatus();
   }
}