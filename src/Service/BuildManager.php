<?php

namespace App\Service;

use App\Entity\Act;
use App\Entity\Build;
use App\Repository\ActRepository;
use App\Repository\BuildRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BuildManager extends AbstractManager
{
    private $params;

    /** @var  BuildRepository */
    protected $repository;

    /** @var  ActRepository */
    protected $actRepository;

    /** @var  ActManager */
    protected $actManager;

    public function __construct(
        EntityManagerInterface $em,
        BuildRepository $repository,
        ParameterBagInterface $params,
        ActRepository $actRepository,
        ActManager $actManager
    ) {
        parent::__construct($em, $repository);
        $this->params = $params;
        $this->actRepository = $actRepository;
        $this->actManager = $actManager;
    }

    public function add($code, $name, $short, $list, $status, UploadedFile $file)
    {
        $fileName = tempnam($this->params->get('dir_files'), 'cp');
        rename($file->getRealPath(), $fileName);
        $build = new Build($code, $name, $short, $status, basename($fileName));
        $this->save($build);

        return filesize($this->params->get('dir_files').DIRECTORY_SEPARATOR.$build->getFile());
    }

    public function getFile($code, $status)
    {
        $builds = $this->repository->findBy(['code' => $code, 'status' => $status], ['added' => 'desc']);
        if (empty($builds)) {
            return false;
        }
        /** @var Build $build */
        $build = $builds[0];

        return $this->params->get('dir_files').DIRECTORY_SEPARATOR.$build->getFile();
    }

    public function getFileById($id)
    {
        /** @var Build $build */
        $build = $this->repository->find($id);

        return $this->params->get('dir_files').DIRECTORY_SEPARATOR.$build->getFile();
    }

    /**
     * @param $status
     *
     * @return string
     */
    public function getList($status)
    {
        $rsm = new ResultSetMapping();
        // build rsm here
        $rsm->addEntityResult('App\Entity\Build', 'b');
        $rsm->addFieldResult('b', 'id', 'id'); // ($alias, $columnName, $fieldName)
        $rsm->addFieldResult('b', 'code', 'code'); // // ($alias, $columnName, $fieldName)
        $rsm->addFieldResult('b', 'name', 'name'); // // ($alias, $columnName, $fieldName)
        $rsm->addFieldResult('b', 'short', 'short'); // // ($alias, $columnName, $fieldName)
        $rsm->addFieldResult('b', 'status', 'status'); // // ($alias, $columnName, $fieldName)
        $rsm->addFieldResult('b', 'added', 'added'); // // ($alias, $columnName, $fieldName)

        $query = $this->em->createNativeQuery('
            SELECT builds.* 
              FROM builds
             INNER JOIN (SELECT code, MAX(added) as m FROM builds WHERE status = :status OR :status_all = :status  GROUP by code, status) AS tb ON builds.code = tb.code
             WHERE builds.added = tb.m',
            $rsm
        );

        $query->setParameter('status_all', 0);
        $query->setParameter('status', $status);
        $builds = $query->getResult();

        $str = '';
        /** @var Build $build */
        foreach ($builds as $build) {
            $str .= sprintf(
                    '%s~%s~%s~%s~%s',
                    $build->getCode(),
                    $build->getName(),
                    $build->getShort(),
                    $build->getAdded()->format('Y-m-d H:i:s'),
                    $build->getStatus()
                ).'#';
        }

        return $str;
    }

    /**
     * @param $code
     * @param $status
     *
     * @return string
     */
    public function getVersion($code, $status)
    {
        $builds = $this->repository->findBy(['code' => $code, 'status' => $status]);

        $str = '';
        /** @var Build $build */
        foreach ($builds as $build) {
            $str .= sprintf(
                    '%u~%s~%s~%s~%s~%s',
                    $build->getCode(),
                    $build->getName(),
                    $build->getShort(),
                    $build->getAdded()->format('Y-m-d H:i:s'),
                    $build->getStatus(),
                    $build->getId()
                ).'#';
        }

        return $str;
    }

    /**
     * @param string $code
     * @param integer $status
     */
    public function delete($code, $status)
    {
        /** @var Build $build */
        if ($status === 0) {
            $builds = $this->repository->findBy(['code' => $code]);
        } else {
            $builds = $this->repository->findBy(['code' => $code, 'status' => $status]);
        }

        foreach ($builds as $build) {
            if($status === 0) {
                $acts = $this->actRepository->findBy(['build' => $build->getCode()]);
                /** @var Act $act */
                foreach ($acts as $act) {
                    $this->actManager->delete($act->getId());
                }
            }

            $file = $this->params->get('dir_files').DIRECTORY_SEPARATOR.$build->getFile();
            if (file_exists($file)) {
                unlink($file);
            }
            $this->remove($build);
            $this->flush();
        }
    }
}