<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActRepository")
 * @ORM\Table(name="acts", uniqueConstraints={@UniqueConstraint(name="act_unique", columns={"code", "build", "status"})})
 */
class Act
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(name="code", type="string", nullable=false)
     */
    protected $code;

    /**
     * @ORM\Column(name="build", type="string", nullable=false)
     */
    protected $build;

    /**
     * @ORM\Column(name="added", type="datetime", nullable=true)
     */
    protected $added;

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    protected $status;

    /**
     * @ORM\Column(name="month", type="date", nullable=false)
     */
    protected $month;

    /**
     * @ORM\Column(name="file", type="string", length=255, nullable=false)
     */
    protected $file;

    public function __construct($codeBuild, $code, $status, $month, $file)
    {
        $this->added = new \DateTime();
        $this->build = $codeBuild;
        $this->code = $code;
        $this->status = $status;
        $this->month = $month;
        $this->file = $file;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getBuild(): ?string
    {
        return $this->build;
    }

    public function setBuild(string $build): self
    {
        $this->build = $build;

        return $this;
    }

    public function getAdded(): ?\DateTimeInterface
    {
        return $this->added;
    }

    public function setAdded(?\DateTimeInterface $added): self
    {
        $this->added = $added;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getMonth(): ?\DateTimeInterface
    {
        return $this->month;
    }

    public function setMonth(\DateTimeInterface $month): self
    {
        $this->month = $month;

        return $this;
    }
}
